Scheme
{
	Fonts
	{
		// TACO CUSTOM FONTS //

		TacoDefault
		{
			"1"
			{
				"name"		"Walkway SemiBold"
				"tall"		"15"
				"weight"	"500"
				"antialias" "1"
			}
		}

		TacoDefaultSmall
		{
			"1"
			{
				"name"		"Walkway SemiBold"
				"tall"		"11"
				"weight"	"500"
				"antialias" "1"
			}
		}

		TacoDefaultMedium
		{
			"1"
			{
				"name"		"Walkway SemiBold"
				"tall"		"14"
				"weight"	"500"
				"antialias" "1"
			}
		}

		TacoBoldSmall
		{
			"1"
			{
				"name"		"Walkway Black"
				"tall"		"11"
				"weight"	"500"
				"antialias" "1"
			}
		}

		TacoSuperBold
		{
			"1"
			{
				"name"		"Walkway Black"
				"tall"		"40"
				"weight"	"500"
				"antialias" "1"
			}
		}
		TacoSuperBoldSmall
		{
			"1"
			{
				"name"		"Walkway Black"
				"tall"		"20"
				"weight"	"500"
				"antialias" "1"
			}
		}

		// CROSSHAIR SHIT //
		TacoCrosshair
		{
			"1"
			{
				"name"	"KnucklesCrosses"
				"tall"	"45"
				"antialias" "1"
				"additive"	"0"
				"outline"	"0"
			}
		}
		TacoCrosshair2
		{
			"1"
			{
				"name"	"KnucklesCrosses"
				"tall"	"15"
				"antialias" "1"
				"additive"	"0"
				"outline"	"0"
			}
		}
		TacoCrosshair3
		{
			"1"
			{
				"name"	"KnucklesCrosses"
				"tall"	"35"
				"antialias" "1"
				"additive"	"0"
				"outline"	"0"
			}
		}

	}
}